#version=RHEL9
# Use text mode install
text
# Do not configure the X Window System
skipx
# Firewall configuration for SSH
firewall --enabled --service=ssh
# No firstboot
firstboot --disable
# Network information
network  --bootproto=dhcp
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8
# Reboot after installation
rootpw --iscrypted [this-is-not-a-root-password]
# SELinux configuration
selinux --enforcing
# System services
services --disabled="kdump" --enabled="sshd"
# License agreement
eula --agreed
# System timezone
timezone --utc Europe/Zurich
timesource --ntp-server ip-time-0.cern.ch
timesource --ntp-server ip-time-1.cern.ch
timesource --ntp-server ip-time-2.cern.ch
# this should not be neeed
ignoredisk --only-use=vda
# Clear the Master Boot Record
zerombr

# Partitioning and bootloader configuration
# Note: biosboot and efi partitions are pre-created in %pre.
# Based on https://github.com/CentOS/sig-cloud-instance-build/blob/98aa8c6f0290feeb94d86b52c561d70eabc7d942/cloudimg/CentOS-8-x86_64-Azure.ks

bootloader --location=mbr --timeout=1 --append="console=ttyS0,115200 console=tty0" --boot-drive=vda
# This is replaced by the pre-creation of partitions by sgdisk
#part biosboot --onpart=vda14 --size=4
part /boot/efi --onpart=vda15 --fstype=vfat --label=EFI
# We specify a minimum size for root. Do not use --grow as some people might want to avoid it with https://clouddocs.web.cern.ch/using_openstack/contextualisation.html#dont-grow-the-underlying-partition
# growpart will take care of resizing it anyway by default if not disabled
part / --fstype="xfs" --size=3000 --mkfsoptions="-n ftype=1" --label=ROOT

%pre --log=/var/log/anaconda/pre-install.log --erroronfail
ARCH=`uname -m`
cat >> /etc/rsyslog.conf  <<DELIM
\$template AnacondaTemplate, "<%PRI%>%TIMESTAMP:::date-rfc3339% image:alma9-cloud-$ARCH %syslogtag:1:32%%msg:::sp-if-no-1st-sp%%msg%"
\$ActionForwardDefaultTemplate AnacondaTemplate

module(load="imfile" mode="inotify")
input(type="imfile"
  File="/tmp/anaconda.log"
  Tag="anaconda")
input(type="imfile"
  File="/tmp/dnf.librepo.log"
  Tag="dnf-librepo")
input(type="imfile"
  File="/tmp/packaging.log"
  Tag="packaging")
input(type="imfile"
  File="/var/log/anaconda/pre-install.log"
  Tag="ks-pre")
input(type="imfile"
  File="/mnt/sysroot/root/anaconda-post.log"
  Tag="ks-post")

*.* action(type="omfwd" queue.type="direct" target="linuxsoftadm.cern.ch" port="5014" protocol="tcp")
DELIM
/usr/bin/systemctl restart rsyslog

# Pre-create the biosboot and EFI partitions
#  - Ensure that efi and biosboot are created at the start of the disk to
#    allow resizing of the OS disk.
#  - Label biosboot and efi as vda14/vda15 for better compat - some tools
#    may assume that vda1/vda2 are '/boot' and '/' respectively.
sgdisk --clear /dev/vda
sgdisk --new=14:2048:10239 /dev/vda
# Size must be 50MB at least
sgdisk --new=15:10240:550M /dev/vda
# Add hex codes to label partitions appropriately as KS part command does
# EF02 hex code: BIOS boot partition
sgdisk --typecode=14:EF02 /dev/vda
# EF00 hex code: EFI System
sgdisk --typecode=15:EF00 /dev/vda
%end

reboot

# External repos are added by Koji tags
%packages --exclude-weakdeps
@^minimal-environment
-biosdevname
-iprutils
-iwl1000-firmware
-iwl100-firmware
-iwl105-firmware
-iwl135-firmware
-iwl2000-firmware
-iwl2030-firmware
-iwl3160-firmware
-iwl3945-firmware
-iwl4965-firmware
-iwl5000-firmware
-iwl5150-firmware
-iwl6000-firmware
-iwl6000g2a-firmware
-iwl6050-firmware
-iwl7260-firmware
CERN-CA-certs
dracut-config-generic
-dracut-config-rescue
cern-dracut-conf
cern-get-keytab
cloud-init
cloud-utils-growpart
-subscription-manager
-libdnf-plugin-subscription-manager
epel-release
krb5-workstation
nfs-utils
grub2
mdadm
# For cases where the bootloader needs to be relocated, like Software RAID setups, we need grub2-efi-x64-modules and efibootmgr
grub2-efi-x64-modules
efibootmgr
parted
# Needed for manual partitioning for dual booting
gdisk
rsync
tar
vim-enhanced
yum-utils
%end

%post --erroronfail --log=/root/anaconda-post.log

# Stop dnf from installing weak dependencies
echo "install_weak_deps=False" >> /etc/dnf/dnf.conf

# lock root account
passwd -d root
passwd -l root

# Generate new machine-id on first boot
truncate --size 0 /etc/machine-id

rpm -e linux-firmware

# cannot login on the console anyway ...
sed -i 's/^#NAutoVTs=.*/NAutoVTs=0/' /etc/systemd/logind.conf
# installation time shutdown problem ?
systemctl restart systemd-logind

# disable cloud-init managing network (new in 7.4)
cat >> /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg << EOF
network: {config: disabled}
EOF

# initscripts don't like this file to be missing.
cat > /etc/sysconfig/network << EOF
NETWORKING=yes
NOZEROCONF=yes
EOF

# simple eth0 config, again not hard-coded to the build hardware
rm -f /etc/sysconfig/network-scripts/ifcfg-*
cat > /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
NAME="eth0"
DEVICE="eth0"
ONBOOT="yes"
NETBOOT="yes"
IPV6INIT="yes"
BOOTPROTO="dhcp"
TYPE="Ethernet"
DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6_AUTOCONF="yes"
IPV6_DEFROUTE="yes"
IPV6_FAILURE_FATAL="no"
DHCPV6_DUID="llt"
USERCTL="yes"
PEERDNS="yes"
PERSISTENT_DHCLIENT="1"
EOF

sed -i 's|^HWADDR=.*||' /etc/sysconfig/network-scripts/ifcfg-eth0
rm -f /etc/udev/rules.d/70-persistent-net.rules

# Make sure we do not leave any remnants of ens3 behind
# This exists because we build with net.ifnames=1, but boot with net.ifnames=0
rm -f /etc/NetworkManager/system-connections/ens3.nmconnection

# and identical chrony.keys (INC0980266)
echo "" > /etc/chrony.keys

#no tmpfs for /tmp."
systemctl disable tmp.mount

# root - enabled, cloud user - disabled.
if [ -e /etc/cloud/cloud.cfg ]; then
    sed -i 's|^disable_root: .*|disable_root: 0|' /etc/cloud/cloud.cfg
    sed -i 's|\- default||' /etc/cloud/cloud.cfg
    sed -i 's|^users:||' /etc/cloud/cloud.cfg

    # fix cloud-init (INC0974035 RH 01594925)
    sed -i -e 's/\&\s\~/\& stop/' /etc/rsyslog.d/21-cloudinit.conf
fi

# default rpm keys imported.
rpm --import /etc/pki/rpm-gpg/*


# clean up installation
rm -f /tmp/{ks-script-*,yum.log}
rm -f /root/{anaconda,original}-ks.cfg
rm -rf /var/cache/dnf/*

# Add the root UUID to the kernel parameters
ROOT_ID=`blkid -s UUID -o value /dev/vda1`
grubby --update-kernel=ALL --args "root=UUID=${ROOT_ID}"

# Add rd.auto and net.ifnames=0 for Ironic
grubby --update-kernel=ALL --args rd.auto
grubby --update-kernel=ALL --args net.ifnames=0

# Set appropriate values for crashkernel from el9 onwards
# Ref. https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9-beta/html/managing_monitoring_and_updating_the_kernel/configuring-kdump-on-the-command-line_managing-monitoring-and-updating-the-kernel
grubby --update-kernel=ALL --args crashkernel=1G-4G:192M,4G-64G:256M,64G-:512M

# We are installing bootloader with the bootloader command, but this only adds bios compatible (as we are building in a UEFI VM), so we force both: https://unix.stackexchange.com/questions/273329/can-i-install-grub2-on-a-flash-drive-to-boot-both-bios-and-uefi
# Enable BIOS bootloader
grub2-mkconfig --output /etc/grub2-efi.cfg
grub2-install --target=i386-pc --directory=/usr/lib/grub/i386-pc/ /dev/vda
# Notes regarding dual booting and grub configuration!!!
# On a BIOS machine grub configuration will be /boot/grub2/grub.cfg, on a UEFI one, /boot/efi/EFI/centos/grub.cfg
# Grub Environmental Block (grubenv) on a BIOS machine is /boot/grub2/grubenv and /boot/efi/EFI/centos/grubenv on UEFI, BUT since
# we are building on a UEFI VM, /boot/grub2/grubenv will always be a symlink to /boot/efi/EFI/centos/grubenv, i.e. both booting modes
# will be using the same file which is on the EFI partition. This means we will need this EFI partition (#15) even on BIOS machines!
grub2-mkconfig --output=/boot/grub2/grub.cfg



# Fix grub.cfg to remove EFI entries and adapt them to BIOS

# We need to adapt grub configuration in the case of BIOS config as it is preloaded with the UEFI partition ID instead of the BIOS partition ID
EFI_ID=`blkid -s UUID -o value /dev/vda15`
BOOT_ID=`blkid -s UUID -o value /dev/vda1`
sed -i 's/gpt15/gpt1/' /boot/grub2/grub.cfg
sed -i "s/${EFI_ID}/${BOOT_ID}/" /boot/grub2/grub.cfg
# Force Grub Environmental Block to come from our common grubenv location
sed -i 's|${config_directory}/grubenv|(hd0,gpt15)/efi/centos/grubenv|' /boot/grub2/grub.cfg
sed -i '/^### BEGIN \/etc\/grub.d\/30_uefi/,/^### END \/etc\/grub.d\/30_uefi/{/^### BEGIN \/etc\/grub.d\/30_uefi/!{/^### END \/etc\/grub.d\/30_uefi/!d}}' /boot/grub2/grub.cfg

# Remove the hostonly dracut config, we only need it for the first initramfs (which has already been created at this point)
yum remove -y dracut-config-generic

# stamp the build
cat << EOF > /etc/BUILDTIME
AlmaLinux 9 - CERN edition
Built on: `date`

 ⠀⠀⠀⠀⠀⠀⢱⣆⠀⠀⠀⠀⠀⠀
 ⠀⠀⠀⠀⠀⠀⠈⣿⣷⡀⠀⠀⠀⠀
 ⠀⠀⠀⠀⠀⠀⢸⣿⣿⣷⣧⠀⠀⠀
 ⠀⠀⠀⠀⡀⢠⣿⡟⣿⣿⣿⡇⠀⠀
 ⠀⠀⠀⠀⣳⣼⣿⡏⢸⣿⣿⣿⢀⠀
 ⠀⠀⠀⣰⣿⣿⡿⠁⢸⣿⣿⡟⣼⡆
 ⢰⢀⣾⣿⣿⠟⠀⠀⣾⢿⣿⣿⣿⣿
 ⢸⣿⣿⣿⡏⠀⠀⠀⠃⠸⣿⣿⣿⡿
 ⢳⣿⣿⣿⠀⠀⠀⠀⠀⠀⢹⣿⡿⡁
 ⠀⠹⣿⣿⡄⠀⠀⠀⠀⠀⢠⣿⡞⠁
 ⠀⠀⠈⠛⢿⣄⠀⠀⠀⣠⠞⠋⠀⠀
 ⠀⠀⠀⠀⠀⠀⠉⠀⠀⠀⠀⠀⠀⠀
EOF
%end

%post --nochroot
# Make rsyslog send everything before the reboot
pkill -HUP rsyslogd
sleep 30s
rm -f /mnt/sysroot/root/anaconda-{ks,pre,post}.log
%end
