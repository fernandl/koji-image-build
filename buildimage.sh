#!/bin/bash

#temp script: TBD better.

function usage {
 echo "$(basename $0) major {release}"
 echo "         major = 7,8s,8sa,8al,8el,9al,9el"
 echo "         release = scratch/release"
}

[ -z $1 ] && usage && exit 1
[ -z $2 ] && usage && exit 1
SCRATCH="--scratch"
[ $2 == "release" ] && SCRATCH=""
VER=$1

URL="http://linuxsoft.cern.ch/cern"
CI_PROJECT_PATH="${CI_PROJECT_PATH:-linuxsupport/koji-image-build}"
CI_COMMIT_SHORT_SHA="${CI_COMMIT_SHORT_SHA:-master}"
KSURL="git+ssh://git@gitlab.cern.ch:7999/${CI_PROJECT_PATH}#${CI_COMMIT_SHORT_SHA}"
FORMAT="raw"
DATE=$(date "+%Y%m%d")
EXTRA_ARGS="--factory-parameter=generate_icicle False"
ARCH="x86_64"
DISK_SIZE="4"

case $VER in
    7)
        NAME="cc7-cloud"
        TAG="cc7-image-7x"
        URL="${URL}/centos/7/os/\$arch"
        DISTRO="RHEL-7.9"
        KSVER="RHEL7"
        ARCH="x86_64"
    ;;
    8s|8sa)
        NAME="cs8-cloud"
        TAG="cs8-image-8x"
        URL="${URL}/centos/s8/BaseOS/\$arch/os/"
        DISTRO="RHEL-8.3"
        KSVER="RHEL8"
    ;;
    8al|8ala)
        NAME="alma8-cloud"
        TAG="alma8-image-8x"
        URL="${URL}/alma/8/BaseOS/\$arch/os/"
        DISTRO="RHEL-8.3"
        KSVER="RHEL8"
    ;;
    8el|8ela)
        NAME="rhel8-cloud"
        TAG="rhel8-image-8x"
        URL="${URL}/rhel/8/baseos/\$arch/os/"
        DISTRO="RHEL-8.3"
        KSVER="RHEL8"
    ;;
    9al|9ala)
        NAME="alma9-cloud"
        TAG="alma9-image-9x"
        URL="${URL}/alma/9/BaseOS/\$arch/os/"
        DISTRO="RHEL-9.0"
        KSVER="RHEL9"
    ;;
    9el|9ela)
        NAME="rhel9-cloud"
        TAG="rhel9-image-9x"
        URL="${URL}/rhel/9/baseos/\$arch/os/"
        DISTRO="RHEL-9.0"
        KSVER="RHEL9"
    ;;
esac

KSFILE="${NAME}.ks"

if [[ "${VER: -1}" == "a" ]]; then
  ARCH="aarch64"
fi

echo "executing: koji -p ${_KOJI_PROFILE} image-build --wait ${NAME} ${DATE} ${TAG} ${URL} ${ARCH} --ksurl=${KSURL} --kickstart=${KSFILE} --distro=${DISTRO} --format=${FORMAT} --disk-size=${DISK_SIZE} --ksversion=${KSVER} ${EXTRA_ARGS} ${SCRATCH}"
sleep 3
koji -p ${_KOJI_PROFILE} image-build --wait ${NAME} ${DATE} ${TAG} ${URL} ${ARCH} --ksurl=${KSURL} --kickstart=${KSFILE} --distro=${DISTRO} --format=${FORMAT} --disk-size=${DISK_SIZE} --ksversion=${KSVER} ${EXTRA_ARGS} ${SCRATCH}
