#!/bin/bash

#temp script: TBD better.

function usage {
 echo "$(basename $0) major test {date} {rel}"
 echo "         major = 7,8s,8sa,8al,8el,9al,9el"
 echo "         test = test/prod"
 echo "         date = YYYYMMDD"
 echo "         rel = 1"
}

[ -z $1 ] && usage && exit 1
[ -z $2 ] && usage && exit 1

VER=${1,,}
ARCHS=""
if [[ "${VER: -1}" == "a" ]]; then
    ARCHS="aarch64"
    VER="${VER:0:-1}"
fi
if [ x$2 == "xprod" ]; then
    os_edition='Base'
    OSEDITION=""
else
    os_edition='Test'
    OSEDITION="TEST"
fi
[ -z $3 ] && KOJIIMGDATE=$(date "+%Y%m%d") || KOJIIMGDATE=$3
[ -z $4 ] && KOJIIMGREL=1 || KOJIIMGREL=$4

UPLDAYDATE=$(/bin/date "+%Y-%m-%d" --date="$KOJIIMGDATE")
FORMAT="raw"
hw_firmware_type="bios"
hw_machine_type="pc"

case $VER in
    7)
        [ x$ARCHS == "x" ] && ARCHS="x86_64"
        os_distro="CC"
        centos_test_cleanup="true"
        IMGPREFIX="cc7-cloud"
    ;;
    8s)
        [ x$ARCHS == "x" ] && ARCHS="x86_64"
        os_distro="CS"
        centos_test_cleanup="true"
        IMGPREFIX="cs8-cloud"
        hw_firmware_type="uefi"
        hw_machine_type="q35"
    ;;
    8al)
        [ x$ARCHS == "x" ] && ARCHS="x86_64"
        os_distro="ALMA"
        centos_test_cleanup="true"
        IMGPREFIX="alma8-cloud"
        hw_firmware_type="uefi"
        hw_machine_type="q35"
    ;;
    8el)
        [ x$ARCHS == "x" ] && ARCHS="x86_64"
        os_distro="RHEL"
        centos_test_cleanup="true"
        IMGPREFIX="rhel8-cloud"
        hw_firmware_type="uefi"
        hw_machine_type="q35"
    ;;
    9al)
        [ x$ARCHS == "x" ] && ARCHS="x86_64"
        os_distro="ALMA"
        centos_test_cleanup="true"
        IMGPREFIX="alma9-cloud"
        hw_firmware_type="uefi"
        hw_machine_type="q35"
    ;;
    9el)
        [ x$ARCHS == "x" ] && ARCHS="x86_64"
        os_distro="RHEL"
        centos_test_cleanup="true"
        IMGPREFIX="rhel9-cloud"
        hw_firmware_type="uefi"
        hw_machine_type="q35"
    ;;
esac
if [[ "${ARCHS}" == "aarch64" ]]; then
    # https://its.cern.ch/jira/browse/OS-16240
    hw_machine_type="virt"
fi

[ -x /usr/bin/ai-rc ] && eval $(ai-rc 'IT Linux Support - CI VMs')

if [[ -n "${CI_SERVER_URL}" ]]; then
    KSFILE="--property ks_file=${CI_SERVER_URL}/${CI_PROJECT_PATH}/-/blob/${CI_COMMIT_SHORT_SHA}/${IMGPREFIX}.ks"
fi

for ARCH in ${ARCHS}; do
    release_date="${UPLDAYDATE}T$(date -u '+%H:%M:%S')Z"
    upstream_provider="linux.support@cern.ch"
    [ $ARCH == "i686" ] && FARCH="i386" || FARCH=$ARCH
    img="${IMGPREFIX}-${KOJIIMGDATE}-${KOJIIMGREL}.${FARCH}.${FORMAT}"
    echo "Inspecting ${img}:"
    # If we have an oz log, the image was built at CERN
    if [ -f oz-$ARCH.log ]; then
      if grep -q "packaging:666:centos-release-" oz-$ARCH.log; then
        # CC7 oz logs have a different format
        version=$(sed '/packaging:666:centos-release-/!d; s/.*release-\(\w-\w\).*/\1/' oz-$ARCH.log)
        version="${version/-/.}"
      else
        version=$(sed '/INFO.*Installed: \(centos\|almalinux\|redhat\)\-\(linux\-\|stream\-\)\?release-/!d; s/.*666:\(\w\.\w\)-.*/\1/' oz-$ARCH.log)
      fi
    fi
    if [[ -z "$version" ]]; then
      echo "Unable to figure out the version, something is very wrong."
      exit 1
    fi
    os_distro_major=${version:0:1}
    if [ ${#version} -gt 1 ]; then
      os_distro_minor=$(echo $version | cut -d. -f2)
    else
      os_distro_minor=0
    fi
    if [ X${OSEDITION} == "X" ]; then
        image_name="$os_distro$os_distro_major - ${ARCH}"
    else
        image_name="$os_distro$os_distro_major ${OSEDITION} - ${ARCH}"
    fi
    full_name="$image_name [$UPLDAYDATE]"

    # "Old" releases have "dynamic" names which include the date
    if [[ $os_distro != "ALMA" && $os_distro != "RHEL" ]]; then
        image_name="$full_name"
    fi

    # Show what we're working with, which might be useful for debugging
    # Protip: if you need to inspect the image, you can use this command:
    #    LIBGUESTFS_BACKEND=direct guestfish --ro -a ${img}
    # in the resulting shell, run "run" to start the VM, then "mount /dev/sda1"
    # (or whatever) to mount the filesystem and then you can "cat" whatever you want.
    PARTITIONS=$(LIBGUESTFS_BACKEND=direct virt-filesystems -a ${img} --long --uuid --no-title)
    echo "Partition table:"
    echo "Name      Type       VFS Label Size Parent UUID"
    printf "$PARTITIONS\n"
    echo "-----------------------------------------------"

    if [[ $(printf "$PARTITIONS\n" | wc -l) -eq 1 ]]; then
        # If there's only one partition, it's got to be the root
        rootfs_uuid=$(printf "$PARTITIONS\n" | awk '{print $7}')
    else
        # We define the label to be 'ROOT'.
        rootfs_uuid=$(printf "$PARTITIONS\n" | grep ROOT | awk '{print $7}')
    fi
    if [[ -z "$rootfs_uuid" ]]; then
        echo "Unable to find rootfs UUID, something is very wrong."
        exit 1
    fi
    echo "rootfs_uuid=${rootfs_uuid}"

    openstack image create -f json --container-format bare --disk-format ${FORMAT} \
        --property os="LINUX" \
        --property os_distro="$os_distro" \
        --property os_distro_major="$os_distro_major" \
        --property os_distro_minor="$os_distro_minor" \
        --property release_date="$release_date" \
        --property os_edition="$os_edition" \
        --property gitops="enable" \
        --property centos_test_cleanup="$centos_test_cleanup" \
        --property architecture="${ARCH}" \
        --property hw_architecture="${ARCH}" \
        --property custom_name="$full_name" \
        --property upstream_provider="$upstream_provider" \
        --property name="$image_name" \
        --property rootfs_uuid="$rootfs_uuid" \
        --property hw_firmware_type="$hw_firmware_type" \
        --property hw_machine_type="$hw_machine_type" \
        $KSFILE \
        --file $img \
        "$image_name" | tee upload.json
done
