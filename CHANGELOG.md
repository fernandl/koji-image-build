# CHANGELOG

Unless otherwise stated, newer images do not include changes compared to previous versions.

## 04-05-2020

* `CC7 - x86_64 [2020-05-04]' (669e6ed2-ae57-4077-a934-128a809c71a5)`
  * Build image against CC7.8 tree

* `C8 - x86_64 [2020-05-04] (9fb46ce1-f0e3-453e-b672-72008edbcd5c)`

* Changes:
  * Dual booting images: images can be now used for both BIOS and UEFI machines, both [virtual](https://clouddocs.web.cern.ch/advanced_topics/uefi_support.html) or physical.
    * This change implies that nodes will now have a mounted EFI partition even on BIOS machines.
    * **Partition numbers may have changed, do not use arbitrary partition numbers for your scripts!!**

## 20-05-2020

* `CC7 - x86_64 [2020-05-20] (f92d3a28-f2ec-4e08-87b1-b379443a0d36)`
* `C8 - x86_64 [2020-05-20] (38093a46-e7b8-4900-a039-e414e93325dd)`

* Changes:
  * Partitions will have labels:
    * We have added a `ROOT` label to the root partition
    * For the EFI pertition we added an `EFI` label.
  * [BUGFIX](https://cern.service-now.com/service-portal?id=outage&n=OTG0056607): Avoid root partition autogrowing, as it does not allow to disable [growpart for root](https://clouddocs.web.cern.ch/using_openstack/contextualisation.html#dont-grow-the-underlying-partition) for manual partitioning through cloud-init.


## 09-07-2020

* `CC7 TEST - x86_64 [2020-07-09] (2d862837-7431-4cb4-bd9a-f8ad1e2f3c15)`
  
* `C8 TEST - x86_64 [2020-07-09] (597d7df9-793c-46e7-b642-2bcaed774ddf)`
  * We now enforce `nvme` driver as it was causing such disks not to be recognised when booting on new hardware.

* Changes:
  * Increasing EFI partition to 550MB
