# How to have a machine able to run the RHEL scripts

Note: As of August 2021, these instructions should no longer be neccessary.
Everything should run correctly via gitlab-ci jobs

I'll save you some headaches.

This is what was needed to run the required scripts form a CentOS 8 machine. The adaptations for Stream 8 should mostly come in the Koji tag urls.

```
yum install libguestfs-tools
export LIBGUESTFS_DEBUG=1 LIBGUESTFS_TRACE=1
export LIBGUESTFS_BACKEND=direct
yum install qemu-kvm libvirt
systemctl enable libvirtd
systemctl start libvirtd
# This next line was required for CC7, but no need for C8
sudo adduser $USER libvirt
sudo systemctl enable virtlogd.socket
sudo systemctl restart virtlogd.socket
# You need certain repos for the dependencies, base your repos on https://gitlab.cern.ch/ai-config-team/ai-tools/-/blob/master/.gitlab-ci.yml
# Basically you may need to update the openstack clients version, ussuri for this time
# Remember to adapt urls when using stream, i.e. hw8s-qa instead of hw8-qa and so on
dnf config-manager --add-repo http://linuxsoft.cern.ch/cern/centos/8/cloud/x86_64/openstack-ussuri/ --nogpgcheck
dnf config-manager --add-repo http://linuxsoft.cern.ch/internal/repos/openstackclients-ussuri8-qa/x86_64/os/ --nogpgcheck
dnf config-manager --add-repo http://linuxsoft.cern.ch/internal/repos/config8-qa/x86_64/os/ --nogpgcheck
dnf config-manager --add-repo http://linuxsoft.cern.ch/internal/repos/hw8-qa/x86_64/os/ --nogpgcheck
dnf config-manager --add-repo http://linuxsoft.cern.ch/internal/repos/linuxsupport8-stable/x86_64/os/ --nogpgcheck
yum install ai-tools
eval $(ai-rc "IT Linux Support - CI VMs")
# Go to the web interface for Openstack and on the right top corner, search for the RC file.
# Take that .sh script and make it executable
chmod 777 ./IT\ Linux\ Support\ -\ CI\ VMs-openrc.sh
./IT\ Linux\ Support\ -\ CI\ VMs-openrc.sh
# You may need this conf file, take it from aiadm
vi /etc/ai/ai.conf
# To try whether you can connect to Openstack, try listing your servers
openstack server list
# If you can, ready to go, you can go back to the main docs.
